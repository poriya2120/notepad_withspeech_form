﻿namespace WindowsFormsApplication5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.file = new System.Windows.Forms.ToolStripMenuItem();
            this.new1 = new System.Windows.Forms.ToolStripMenuItem();
            this.open = new System.Windows.Forms.ToolStripMenuItem();
            this.save = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exit = new System.Windows.Forms.ToolStripMenuItem();
            this.edit = new System.Windows.Forms.ToolStripMenuItem();
            this.color = new System.Windows.Forms.ToolStripMenuItem();
            this.font = new System.Windows.Forms.ToolStripMenuItem();
            this.toolbax = new System.Windows.Forms.ToolStrip();
            this.tnew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.topen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsave = new System.Windows.Forms.ToolStripButton();
            this.texsit = new System.Windows.Forms.ToolStripButton();
            this.button6 = new System.Windows.Forms.Button();
            this.menu.SuspendLayout();
            this.toolbax.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(119, 66);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(853, 423);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(1082, 66);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 41);
            this.button1.TabIndex = 1;
            this.button1.Text = "open";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.Location = new System.Drawing.Point(1082, 114);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 41);
            this.button2.TabIndex = 1;
            this.button2.Text = "save";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.Location = new System.Drawing.Point(1082, 161);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(126, 36);
            this.button3.TabIndex = 1;
            this.button3.Text = "font";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Red;
            this.button4.Location = new System.Drawing.Point(1082, 215);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(126, 31);
            this.button4.TabIndex = 1;
            this.button4.Text = "background";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Red;
            this.button5.Location = new System.Drawing.Point(1082, 252);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(126, 31);
            this.button5.TabIndex = 1;
            this.button5.Text = "folder";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(1130, 388);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // menu
            // 
            this.menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.file,
            this.edit});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(950, 28);
            this.menu.TabIndex = 3;
            this.menu.Text = "menuStrip1";
            this.menu.Visible = false;
            // 
            // file
            // 
            this.file.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.new1,
            this.open,
            this.save,
            this.toolStripSeparator1,
            this.exit});
            this.file.Image = ((System.Drawing.Image)(resources.GetObject("file.Image")));
            this.file.Name = "file";
            this.file.Size = new System.Drawing.Size(62, 24);
            this.file.Text = "file";
            // 
            // new1
            // 
            this.new1.Image = ((System.Drawing.Image)(resources.GetObject("new1.Image")));
            this.new1.Name = "new1";
            this.new1.Size = new System.Drawing.Size(118, 26);
            this.new1.Text = "new";
            this.new1.Click += new System.EventHandler(this.new1_Click);
            // 
            // open
            // 
            this.open.Image = ((System.Drawing.Image)(resources.GetObject("open.Image")));
            this.open.Name = "open";
            this.open.Size = new System.Drawing.Size(118, 26);
            this.open.Text = "open";
            this.open.Click += new System.EventHandler(this.open_Click);
            // 
            // save
            // 
            this.save.Image = ((System.Drawing.Image)(resources.GetObject("save.Image")));
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(118, 26);
            this.save.Text = "save";
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(115, 6);
            // 
            // exit
            // 
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(118, 26);
            this.exit.Text = "exit";
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // edit
            // 
            this.edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.color,
            this.font});
            this.edit.Image = ((System.Drawing.Image)(resources.GetObject("edit.Image")));
            this.edit.Name = "edit";
            this.edit.Size = new System.Drawing.Size(67, 24);
            this.edit.Text = "edit";
            // 
            // color
            // 
            this.color.Image = ((System.Drawing.Image)(resources.GetObject("color.Image")));
            this.color.Name = "color";
            this.color.Size = new System.Drawing.Size(118, 26);
            this.color.Text = "color";
            this.color.Click += new System.EventHandler(this.color_Click);
            // 
            // font
            // 
            this.font.Image = ((System.Drawing.Image)(resources.GetObject("font.Image")));
            this.font.Name = "font";
            this.font.Size = new System.Drawing.Size(118, 26);
            this.font.Text = "font";
            this.font.Click += new System.EventHandler(this.font_Click);
            // 
            // toolbax
            // 
            this.toolbax.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolbax.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tnew,
            this.toolStripSeparator2,
            this.topen,
            this.toolStripSeparator3,
            this.tsave,
            this.texsit});
            this.toolbax.Location = new System.Drawing.Point(0, 0);
            this.toolbax.Name = "toolbax";
            this.toolbax.Size = new System.Drawing.Size(1298, 27);
            this.toolbax.TabIndex = 4;
            this.toolbax.Text = "toolStrip1";
            // 
            // tnew
            // 
            this.tnew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tnew.Image = ((System.Drawing.Image)(resources.GetObject("tnew.Image")));
            this.tnew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tnew.Name = "tnew";
            this.tnew.Size = new System.Drawing.Size(24, 24);
            this.tnew.Text = "new";
            this.tnew.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.tnew.ToolTipText = "new notpad";
            this.tnew.Click += new System.EventHandler(this.tnew_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // topen
            // 
            this.topen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.topen.Image = ((System.Drawing.Image)(resources.GetObject("topen.Image")));
            this.topen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.topen.Name = "topen";
            this.topen.Size = new System.Drawing.Size(24, 24);
            this.topen.Text = "open";
            this.topen.ToolTipText = "open notpad";
            this.topen.Click += new System.EventHandler(this.topen_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // tsave
            // 
            this.tsave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsave.Image = ((System.Drawing.Image)(resources.GetObject("tsave.Image")));
            this.tsave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsave.Name = "tsave";
            this.tsave.Size = new System.Drawing.Size(24, 24);
            this.tsave.Text = "save";
            this.tsave.ToolTipText = "save notpad";
            this.tsave.Click += new System.EventHandler(this.tsave_Click);
            // 
            // texsit
            // 
            this.texsit.Image = ((System.Drawing.Image)(resources.GetObject("texsit.Image")));
            this.texsit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.texsit.Name = "texsit";
            this.texsit.Size = new System.Drawing.Size(63, 24);
            this.texsit.Text = "exsit";
            this.texsit.Click += new System.EventHandler(this.texsit_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Red;
            this.button6.Location = new System.Drawing.Point(1082, 300);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(126, 29);
            this.button6.TabIndex = 5;
            this.button6.Text = "read";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(1298, 552);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.toolbax);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menu);
            this.MainMenuStrip = this.menu;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.toolbax.ResumeLayout(false);
            this.toolbax.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem file;
        private System.Windows.Forms.ToolStripMenuItem new1;
        private System.Windows.Forms.ToolStripMenuItem open;
        private System.Windows.Forms.ToolStripMenuItem save;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exit;
        private System.Windows.Forms.ToolStripMenuItem edit;
        private System.Windows.Forms.ToolStripMenuItem color;
        private System.Windows.Forms.ToolStripMenuItem font;
        private System.Windows.Forms.ToolStrip toolbax;
        private System.Windows.Forms.ToolStripButton tnew;
        private System.Windows.Forms.ToolStripButton topen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsave;
        private System.Windows.Forms.ToolStripButton texsit;
        private System.Windows.Forms.Button button6;
    }
}

