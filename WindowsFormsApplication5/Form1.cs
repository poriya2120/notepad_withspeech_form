﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "slect folder";
            folderBrowserDialog1.ShowNewFolderButton = true;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK) {
                label1.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        string filename;
        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "txt file(*.txt)|*.txt|All file (*.*)|*.*";
            openFileDialog1.Title = "note pad open dilog";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = openFileDialog1.FileName;
                textBox1.Text = System.IO.File.ReadAllText(filename);

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            saveFileDialog1.DefaultExt = "txt";
            saveFileDialog1.Filter = "txt file (*.txt)|*.txt|all file(*,*)|*.*";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.Title = "note pad save";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK) {
                filename = saveFileDialog1.FileName;
                System.IO.File.WriteAllText(filename, textBox1.Text);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            fontDialog1.ShowColor = true;
            if (fontDialog1.ShowDialog() == DialogResult.OK) {
                textBox1.Font = fontDialog1.Font;
                textBox1.ForeColor = fontDialog1.Color;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK) {
                textBox1.BackColor = colorDialog1.Color;
            }
        }
    }
}
